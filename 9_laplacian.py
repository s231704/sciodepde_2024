from utils_9 import solve_9
import numpy as np
import matplotlib.pyplot as plt

grid_whidth = [1 / 2**h for h in range(2, 8)]
err = np.zeros(6)
for i in range(2,8):
	print(i)
	h = 1 / 2**i
	err_solve = solve_9(h)[1]
	MSE = np.sum(err_solve ** 2) / len(err_solve)
	err[i-2] = MSE


plt.loglog(np.flip(grid_whidth), np.flip(err))
plt.plot(np.flip(grid_whidth), np.power(grid_whidth, -4))
plt.show()