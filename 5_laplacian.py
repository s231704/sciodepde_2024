from utils_5 import solve_5
import numpy as np
import matplotlib.pyplot as plt

grid_whidth = [1 / 2**h for h in range(2, 8)]
err = np.zeros(6)
for i in range(2,8):
	h = 1 / 2**i
	err_solve = solve_5(h)[1]
	MSE = np.sum(err_solve ** 2) / len(err_solve)
	err[i-2] = MSE

plt.loglog(np.flip(grid_whidth), np.flip(err))
plt.plot(np.flip(grid_whidth), np.power(np.flip(grid_whidth), -4))
plt.show()