import numpy as np
import math
import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
from matplotlib import cm
from matplotlib.ticker import LinearLocator
from mpl_toolkits.mplot3d import axes3d

#dirichlet boundary condition



forcing_term = lambda x, y : -16 * np.pi**2 * (((x**2 + y**2)*np.cos(4*np.pi*x * y)) + 2 * np.sin(4*np.pi * (x + y)))
actual = lambda x, y : np.sin(4* np.pi * (x + y)) + np.cos(4 * np.pi * x * y)

    #grid whidth

def solve_5(h):
    N = int(1 / h) - 1
    print(h)
    x = [0, 1]
    y = [0, 1]

    A = np.zeros((N**2, N**2))
    for y in range(N**2):
        for x in range(N**2):
            if (x == y):
                A[y][x] = -4
                if (x - N >= 0):
                    A[y][x - N] = 1
                if (x >= 1 and x % N != 0):
                    A[y][x - 1] = 1
                if (x + N < N**2):
                    A[y][x + N] = 1
                if (x < N**2-1 and (x+1) % N != 0):
                    A[y][x + 1] = 1

    print(A)

    #Ddiag  = -4 * np.eye(N)
    #Dupper = np.diag([1] * (N - 1), 1)
    #Dlower = np.diag([1] * (N - 1), -1)
    #D = Ddiag + Dupper + Dlower
    #Ds = [D] * (N)
    #A  = sp.linalg.block_diag(*Ds)
    #I = np.ones((N) * (N - 1))
    #Iupper = np.diag(I, N)
    #Ilower = np.diag(I, -N )
    #A += Iupper + Ilower
    #print(A)
    #print(A - A1)
    #print(A@np.array(c))
    A = A / (h**2)


    X_T = np.linspace(0, 1, N + 2)
    Y_T = np.linspace(0, 1, N + 2)


    grid_vector = np.linspace(h, 1-h, N)

    analytical = np.array([actual(x, y) for y in Y_T for x in X_T]).reshape(N+2, N+2)




    X_T = X_T[1:-1]
    Y_T = Y_T[1:-1]

    b = np.array([forcing_term(x, y) for y in Y_T for x in X_T]).reshape((N, N))
    ######### Method 1 for defining boundary conditions ##########
    b[0] -= (analytical[0][1:-1] / h**2)
    b[-1] -= (analytical[-1][1:-1] / h**2)
    b[:, 0] -= (analytical[:, 0][1:-1] / h**2)
    b[:, -1] -= (analytical[:, -1][1:-1] / h**2)

    ######### Method 2 for defining boundary conditions ############
    #b[0] -= [actual(x,0)/h**2 for x in grid_vector]
    #b[-1] -= [actual(x,1)/h**2 for x in grid_vector]
    #b[:,0] -= [actual(0,y)/h**2 for y in grid_vector]
    #b[:,-1] -= [actual(1, y)/h**2 for y in grid_vector]

    b = b.reshape(N**2)
    ########## Method 3 for defining boundary conditions #############
    #for i in range(N**2):
    #    if (i < N):
    #        b[i] -= (actual((i + 1) * h, 0)) / h**2
    #    if (i / N >= N - 1):
    #        b[i] -= (actual((i%N + 1) * h, 1)) / h**2
    #    if (i % N == 0):
    #        b[i] -= (actual(0, (int)(i / N + 1) * h)) / h**2
    #    if ((i+1) % N == 0):
    #        b[i] -= (actual(1, (int)(i / N + 1) * h))  / h**2
    ##################################################################

    print("Solving system")
    Z = (sp.linalg.solve(A, b)).reshape((N, N))

    X_T = np.linspace(0, 1, N + 2)[1:-1]
    Y_T = np.linspace(0, 1, N + 2)[1:-1]

    err = np.abs([Z[i][j] - actual(X_T[i], Y_T[j]) for j in range(len(Y_T)) for i in range(len(X_T))])
    return Z.reshape(N**2), err

    #err.reshape((N, N))

    #fig, ax = plt.subplots(subplot_kw={"projection": "3d"}, nrows=1, ncols=3)
    #print("plotting results")
    #
    #X, Y = np.meshgrid(X_T, Y_T)
    #analytical = np.array([actual(X_T[i], Y_T[j]) for j in range(len(Y_T)) for i in range(len(X_T))]).reshape(N, N)
    #    # Plot the surface.
    #
    ##ax.plot_wireframe(X, Y, Z, rstride=10, cstride=10)
    #ax[0].plot_surface(X, Y, analytical, cmap=cm.coolwarm,
    #                     linewidth=0, antialiased=False)
    #ax[1].plot_surface(X, Y, Z, cmap=cm.coolwarm,
    #                      linewidth=0, antialiased=False)
    #ax[2].plot_surface(X, Y, err.reshape(N, N), cmap=cm.coolwarm,
    #                      linewidth=0, antialiased=False)
    #
    #plt.show()
