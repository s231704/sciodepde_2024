import numpy as np
import math
import matplotlib.pyplot as plt
import scipy as sp


a = 0
b = 1
alpha = -1
beta = 1.5
x_bar = 1/2*(a + b - alpha - beta)
w0 = 1/2*(a - b + beta - alpha)

def solve291(eps,u0):
    N = len(u0)
    h = (b-a)/N

    u = u0

    I = range(1,N-1)
    ex = np.ones(N)
    offsets = np.array([-1, 0, 1])

    k = 1
    while k < 20:
        mG = -np.array([eps/h**2 * (u[i-1]-2*u[i]+u[i+1]) + u[i]/(2*h) * (u[i+1]-u[i-1]-2*h) for i in I])
        Ji = [-2*eps/h**2 + (u[i+1]-u[i-1])/(2*h) - 1 for i in I]
        Jim1 = [eps/h**2 - u[i]/(2*h) for i in I]
        Jip1 = [eps/h**2 + u[i]/(2*h) for i in I]
        data = np.array([Jim1, Ji, Jip1])
        J = sp.sparse.dia_array((data, offsets), shape=(N-2, N-2)).toarray()
        print(J)
        # delta = sp.sparse.linalg.spsolve(J,mG)
        delta = np.linalg.solve(J,mG)
        u[1:N-1] = u[1:N-1] + delta
        k = k + 1
    return x,u

k1 = 11

N1 = 2**k1
eps = 0.01
x = np.linspace(0,1,N1+1)
u0 = x - x_bar + w0 * np.tanh( w0*(x-x_bar)/(2*eps) )
u0[0] = alpha
u0[-1] = beta

x1,u1 = solve291(eps,u0)

x = np.linspace(0,1,N1+1)
x2,u2 = solve291(eps,u1)

N = np.array([2**k for k in range(5,k1)])

e = np.zeros(N.shape)


# eps = 0.08

for i, n in enumerate(N):
    x = np.linspace(0,1,n+1)
    u0 = x - x_bar + w0 * np.tanh( w0*(x-x_bar)/(2*eps) )
    u0[0] = alpha
    u0[-1] = beta
    x,u = solve291(eps,u0)
    d = int(N1/n)
    I = d*np.arange(0,n+1)
    e[i] = 	max(abs((u1[I]-u)))

plt.loglog(1/N,e,'-o')
plt.loglog(1/N,100/N**2,'--')

# plt.plot(x1,u1)

plt.show()

