import numpy as np
import math
import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
from matplotlib import cm
from matplotlib.ticker import LinearLocator
from mpl_toolkits.mplot3d import axes3d


forcing_term = lambda x, y : -16 * np.pi**2 * (((x**2 + y**2)*np.cos(4*np.pi*x * y)) + 2 * np.sin(4*np.pi * (x + y)))
actual = lambda x, y : np.sin(4* np.pi * (x + y)) + np.cos(4 * np.pi * x * y)
#grid whidth
def solve_9(h):
    N = (int)(1 / h)  - 1

    x = [0, 1]
    y = [0, 1]
    A = np.zeros((N**2, N**2))
    for y in range(N**2):
        for x in range(N**2):
            if (x == y):
                A[y][x] = -20
                if (x < N**2-1 and (x+1) % N != 0):
                    A[y][x + 1] = 4
                    if (x - N >= 0):
                        A[y][x - N + 1] = 1
                    if (x + N < N**2 - 1):
                        A[y][x + N + 1] = 1
                if (x >= 1 and x % N != 0):
                    A[y][x - 1] = 4
                    if (x - N >= 0):
                        A[y][x - N - 1] = 1
                    if (x + N < N**2):
                        A[y][x + N - 1] = 1
                if (x + N < N**2):
                    A[y][x + N] = 4
                if (x - N >= 0):
                    A[y][x - N] = 4





    A = A / (6 * h**2)
    X_T = np.linspace(0, 1, N + 2)[1:-1]
    Y_T = np.linspace(0, 1, N + 2)[1:-1]
    b = np.array([forcing_term(x, y) for y in Y_T for x in X_T])
    for i in range(N**2):
        if (i < N):
            b[i] -= 4 * actual((i+1) * h, 0) / (6 * h**2)

            b[i] -= actual(i * h, 0) / (6 * h**2)

            b[i] -= actual((i + 2) * h, 0) / (6 * h**2)



        if (i >= N**2 - N):
            b[i] -= 4 * actual((i % N + 1) * h, 1) / (6 * h**2)
            b[i] -= actual(i % N * h, 1) / (6 * h**2)
            b[i] -= actual((i % N + 2) * h, 1) / (6 * h**2)

        if (i % N  == 0):
            b[i] -= 4 * actual(0, ((int)(i / N + 1) * h)) / (6 * h**2)
            if (i >= N):
                b[i] -= actual(0, (int)(i / N) * h) / (6 * h**2)
            if (i < N**2 - N):
                b[i] -= actual(0, ((int)(i / N + 2) * h)) / (6 * h**2)
        if ((i+1) % N == 0):
            b[i] -= 4 * actual(1, ((int)(i / N + 1) * h)) / (6 * h**2)
            if (i >= N):
                b[i] -= actual(1, (int)(i / N) * h) / (6 * h**2)
            if (i < N**2 - N):
                b[i] -= actual(1, ((int)(i / N + 2) * h)) / (6 * h**2)


    print("Solving system")
    Z = (sp.linalg.solve(A, b)).reshape((N, N))

    err = np.abs([Z[i][j] - actual(X_T[i], Y_T[j]) for j in range(len(Y_T)) for i in range(len(X_T))])

    return Z.reshape(N**2), err

    #fig, ax = plt.subplots(subplot_kw={"projection": "3d"}, nrows=1, ncols=3)
    #print("plotting results")
    #
    #X, Y = np.meshgrid(X_T, Y_T)
    #analytical = np.array([actual(x, y) for y in Y_T for x in X_T]).reshape(N, N)
    #    # Plot the surface.
    #
    ##ax.plot_wireframe(X, Y, Z, rstride=10, cstride=10)
    #ax[0].plot_surface(X, Y, analytical, cmap=cm.coolwarm,
    #                     linewidth=0, antialiased=False)
    #ax[1].plot_surface(X, Y, Z, cmap=cm.coolwarm,
    #                      linewidth=0, antialiased=False)
    #ax[2].plot_surface(X, Y, err.reshape(N, N), cmap=cm.coolwarm,
    #                      linewidth=0, antialiased=False)
    #
    #plt.show()